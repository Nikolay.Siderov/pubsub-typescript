class PubSub {
  private static _subscriptions: {[key: string]: Function[]} = {};

  /**
   * @returns The number of triggered callbacks
   */
  public static publish(subscriptionName: string, data: any): number {
    const subscriptions = this._subscriptions[subscriptionName];

    if (Array.isArray(subscriptions) && subscriptions.length !== 0) {
      subscriptions.forEach(callback => callback(data));
      return subscriptions.length;
    }

    return 0;
  }

  public static subscribe(subscriptionName: string, callback: Function): IPubSubSubcription {
    const subscriptions = this._subscriptions[subscriptionName];

    if (Array.isArray(subscriptions)) {
        subscriptions.push(callback);
    } else {
        this._subscriptions[subscriptionName] = [callback];
    }

    return {
        unsubscribe: () => {
            this._subscriptions[subscriptionName] = this._subscriptions[subscriptionName].filter(x => x !== callback);
        }
    };
  }
}

interface IPubSubSubcription {
    unsubscribe(): void;
}
